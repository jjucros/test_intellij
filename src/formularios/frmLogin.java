package formularios;

import javax.swing.*;

/**
 * Created by Ze on 11/18/2014.
 */
public class frmLogin  extends JFrame {
    //Objetos del formulario
    private JLabel lblUsuario;
    private JLabel lblClave;
    private JLabel lblImagen;
    private JTextField txtUsuario;
    private JPasswordField txtClave;
    private JButton btnAceptar;
    private JButton btnCancelar;
    


    //Constructor
    public frmLogin(){
        //Propiedades del formulario
        setTitle("Ingreso al sistema");
        setResizable(false);
        setSize(390,180);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Creamos los iconos
        Icon icoAceptar = new ImageIcon(getClass().getResource("/images/Aceptar2.png"));
        Icon icoCancelar = new ImageIcon(getClass().getResource("/images/Cancelar.png"));
        Icon icoImagen = new ImageIcon(getClass().getResource("/images/Ingreso.png"));

        //Creamos objetos del formulario
        lblUsuario = new JLabel("Usuario");
        txtUsuario = new JTextField(10);

        lblClave= new JLabel("Clave");
        txtClave= new JPasswordField(10);

        btnAceptar= new JButton("Aceptar", icoAceptar);
        btnCancelar= new JButton("Cancelar", icoCancelar);

        //Definimos propiedades de los objetos
        btnAceptar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnAceptar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnAceptar.setToolTipText("Ingresa al Sistema");

        btnCancelar.setHorizontalTextPosition(SwingConstants.HORIZONTAL);
        btnCancelar.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnCancelar.setToolTipText("Cierra la operación y la aplicación ");

        lblImagen = new JLabel(icoImagen);

        //Adicionamos objetos al formulario
        add(lblUsuario);
        add(txtUsuario);
        add(lblClave);
        add(txtClave);
        add(btnAceptar);
        add(btnCancelar);
        add(lblImagen);

        //Ubicar lo objetos en el formulario
        lblUsuario.reshape(20, 20, 100, 20);
        txtUsuario.reshape(120, 20, 100, 20);

        lblImagen.reshape(250, 20, 128, 128);

        lblClave.reshape(20, 45, 100, 20);
        txtClave.reshape(120, 45, 100, 20);

        btnAceptar.reshape(20, 75, 90, 60);
        btnCancelar.reshape(120, 75, 90, 60);


    }


}
